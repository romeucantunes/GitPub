![logo](https://user-images.githubusercontent.com/18054053/44956569-63f60980-ae9c-11e8-88c3-b67ba48f4693.png)

## About the Project

GitPub is an web application developed by Software Engineering students of Universidade de Brasilia - UnB, that aims to aid the management of software projects within university disciplines.
The application consists of a repository for projects that are developed in the course of the disciplines of the UnB and that most of the time are lost for lack of an appropriate place to expose what has been developed by the students and as a result the University loses the opportunity to own a portfolio with quality projects.
The repository will be connected with the platforms of source code hosting with version control GitHub And GitLab.

## Installation

OS X & Linux:

It is required to have Python 3 and Django installed.
Navigate to the project folder, run migrations and then the server.

```
cd GitPub/
python3 ./manage.py migrate
python3 ./manage.py runserver 0.0.0.0:8000
```

Docker:

## Main functionalities

- View Disciplines / Semesters / Projects
- Register Users
- Log in Users
- Comment
- Enroll in Discipline
- Register Projects
- Create Disciplines
- Remove Students
- Manage Disciplines

## Create an user
To create an user via command line, you have to access the web container bash:
```
docker exec -it gitpub_web_1 bash
```

Inside the container, run the command to create a super user:
```
python3 manage.py createsuperuser
```

Fill the required attributes and the user should be created. Your user will have admin privileges, including access to Django Admin interface (/admin)


## Team

|         Nome          |               Email               |                 GitHub                                              |
|:---------------------:|:---------------------------------:|:-------------------------------------------------------------------:|
|  Arthur Diniz         |  [arthurbdiniz@gmail.com]()       |   [@arthurbdiniz](https://github.com/arthurbdiniz)                  |
|  Diego Resende        |  [diegowdrl@gmail.com]()          |   [@wdresende](https://github.com/wdresende)                        |
|  Gabriel Ziegler      |  [gabrielziegler3@gmail.com]()    |   [@gabrielziegler3](https://github.com/gabrielziegler3)            |
|  Joao Carlos          |  [joao4018@gmail.com]()           |   [@joao4018](https://github.com/joao4018)                          |
|  Kamila Costa         |  [kamillacosta.unb@gmail.com]()   |   [@KahCosta](https://github.com/KahCosta)                          |
|  Romeu Carvalho       |  [romeucarvalho2009@hotmail.com]()|   [@RomeuCarvalhoAntunes](https://github.com/RomeuCarvalhoAntunes)  |
|  Vitor Falcão         |  [vitorfhcosta@gmail.com]()       |   [@vitorfhc](https://github.com/vitorfhc)                          |
|  Vitor Moura          |  [victor_cmoura@hotmail.com]()    |   [@victorcmoura](https://github.com/victorcmoura)                  |

## Documents

* **Políticas**
* [Folha de Estilo](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Folha%20de%20Estilo.md)
* [Branches](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Branches.md)
* [Commits](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Commits.md)
* [Issue](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Issues.md)
* [Pull Request](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Pull%20Request.md)

* **Dinâmica 1**
  * [Priorização Documentos](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Priorizacao-Documentos.md)
  * [Rich Picture](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Richpicture.md)
  * [Argumentação](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Argumentacao.md)
  * [Brainstorm](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Brainstorm.md)
  * [Questionário](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Questionario.md)
  * [Moscow](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Moscow.md)
  * [Storytelling](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Storytelling.md)
  * [Cenários](https://github.com/Desenho2018-2/GitPub/blob/master/docs/cenarios.md)
  * [Lexicos](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Lexicos.md)
  * [Protótipo Baixa Fidelidade](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Prototipo-Baixa-Fidelidade.md)

* **Dinâmica 2**
  * [Metodologia](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Metodologia.md)

* **Dinâmica 3**
  * [Product Backlog](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Product-Backlog.md)
  * [Diagrama de Casos de Uso](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Diagrama-caso-de-uso.md)
  * [Diagrama de Classe](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Diagrama-de-Classe.md)
  * [Diagrama Sequencial](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Diagrama-Sequencial.md)
  * [Diagrama de Pacotes](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Diagrama-Pacotes.md)
  * [iStar](https://github.com/Desenho2018-2/GitPub/blob/master/docs/iStar.md)
    * [Strategic Dependency](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Strategic-Dependency.md)
    * [Strategic Rationale](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Strategic-Rationale.md)
  * [NFR Framework](https://github.com/Desenho2018-2/GitPub/blob/master/docs/NFRFramework.md)
  * [Banco de Dados](https://github.com/Desenho2018-2/GitPub/blob/master/docs/banco-de-dados.md)
  * [Matrizes de Rastreabilidade](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Matrizes-de-rastreabilidade.md)
  * [Checklist](https://github.com/Desenho2018-2/GitPub/blob/master/docs/checklist.md)
