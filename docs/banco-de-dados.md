### Histórico de Revisões

| Data       | Versão | Descrição            |         Autor             |
|:----------:|:------:|:--------------------:|:-------------------------:|
| 03/09/2018 | 1.0 | Criação do ME-R e DE-R  | [Vitor Falcão](https://github.com/vitorfhc) |
| 07/09/2018 | 1.1 | Adicionando DE-R Versão 2  | [Victor Moura](https://github.com/victorcmoura) |
| 13/09/2018 | 1.2 | Adicionando DE-R Versão 3  | [Victor Moura](https://github.com/victorcmoura) |

---
## Versão 3
### DE-R
![de-r](https://github.com/Desenho2018-2/GitPub/blob/master/docs/images/DER_GitPub_v3.jpg?raw=true)

## Versão 2
### DE-R
![de-r](https://github.com/Desenho2018-2/GitPub/blob/04a8047cd774cf76caa4bd97cb1f9e6609527444/docs/images/DER_GitPub_v2.jpg?raw=true)

## Versão 1
### DE-R
![de-r](https://github.com/Desenho2018-2/GitPub/blob/04a8047cd774cf76caa4bd97cb1f9e6609527444/docs/images/DER_GitPub_v1.png?raw=true)

