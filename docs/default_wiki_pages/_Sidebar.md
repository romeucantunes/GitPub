![logo](https://user-images.githubusercontent.com/18054053/44956569-63f60980-ae9c-11e8-88c3-b67ba48f4693.png)

* **Políticas**
* [Folha de Estilo](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Folha%20de%20Estilo.md)
* [Branches](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Branches.md)
* [Commits](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Commits.md)
* [Issue](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Issues.md)
* [Pull Request](https://github.com/Desenho2018-2/GitPub/blob/master/docs/policies/Pull%20Request.md)

* **Dinâmica 1**
  * [Priorização Documentos](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Priorizacao-Documentos.md)
  * [Rich Picture](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Richpicture.md)
  * [Argumentação](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Argumentacao.md)
  * [Brainstorm](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Brainstorm.md)
  * [Questionário](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Questionario.md)
  * [Moscow](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Moscow.md)
  * [Storytelling](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Storytelling.md)
  * [Cenários](https://github.com/Desenho2018-2/GitPub/blob/master/docs/cenarios.md)
  * [Lexicos](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Lexicos.md)
  * [Protótipo Baixa Fidelidade](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Prototipo-Baixa-Fidelidade.md)

* **Dinâmica 2**
  * [Metodologia](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Metodologia.md)

* **Dinâmica 3**
  * [Product Backlog](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Product-Backlog.md)
  * [Diagrama de caso de uso](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Diagrama-caso-de-uso.md)
  * [Diagrama de Classe](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Diagrama-de-Classe.md)
  * [iStar](https://github.com/Desenho2018-2/GitPub/blob/master/docs/iStar.md)
    * [Strategic Dependency](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Strategic-Dependency.md)
    * [Strategic Rationale](https://github.com/Desenho2018-2/GitPub/blob/master/docs/Strategic-Rationale.md)
  * [NFR Framework](https://github.com/Desenho2018-2/GitPub/blob/master/docs/NFRFramework.md)
  * [Banco de Dados](https://github.com/Desenho2018-2/GitPub/blob/master/docs/banco-de-dados.md)
  * [Protótipo Alta Fidelidade]()