### Histórico de Revisões

| Data       | Versão | Descrição            |         Autor             |
|:----------:|:------:|:--------------------:|:-------------------------:|
| 14/09/2018 | 0.1 | Criação do Documento  | [Vitor Falcão](https://github.com/vitorfhc) |

---

## Versão 1
![diagrama-pacotes](images/packages_diagram.png)
