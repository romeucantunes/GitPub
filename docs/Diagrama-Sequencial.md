| Data       | Versão | Descrição               | Autor             |
|:----------:|:------:|:-----------------------:|:-----------------:|
| 13/09/2018 | 1.0    | Criação do documento    | Kamilla Costa     |
| 13/09/2018 | 1.1    | Adicionando Diagramas   | Kamilla Costa     |

## Sumário
1. [Introdução](#1-introdução)  
2. [Diagramas](#2-diagramas) <br>
2.1. [Login Aluno](##21-login-aluno) <br>
2.2. [VisualizarDisciplina](##22-visualizar-disciplina) <br>
2.3. [Vinculo e Desvinculo de disciplina](##23-vinculo-e-desvinculo-de-disciplina)


## 1. Introdução
&emsp; O Diagrama de Sequência tem como finalidade representar como as mensagens entre os objetos são trocadas no decorrer do tempo para realizar uma operação.Ele é construído a partir do Diagrama de Casos de Uso, onde primeiro define-se qual o papel do sistema (GitPub) e, em seguida, como o sistema realizará seu papel (sequência das operações).O Diagrama de Sequência dá destaque a ordem temporal em que as mensagens são trocadas entre os objetos e atores de um sistema. Utilizamos o diagrama de sequência para modelar a lógica dos principais processos do sistema GitPub.

## 2. Diagramas
&emsp;

### 2.1. Login Aluno
![Login Aluno](images/DiagramaSequencialLoginAlunov1.png)

### 2.2. Visualizar Disciplina
![Visualizar Disciplina](images/DiagramaSequencialBuscarDisciplinav1.png)

### 2.3. Vinculo e Desvinculo de disciplina
![Vinculo/Desvinculo de Disciplina](images/DiagramaSequencialVincularDisciplinav1.png)
