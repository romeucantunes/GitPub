![logo](https://user-images.githubusercontent.com/18054053/44956569-63f60980-ae9c-11e8-88c3-b67ba48f4693.png)

* **Políticas**
* [Folha de Estilo](https://github.com/Desenho2018-2/GitPub/docs/Folha-de-Estilo.md)
* [Branches](https://github.com/Desenho2018-2/GitPub/docs/Branches.md)
* [Commits](https://github.com/Desenho2018-2/GitPub/docs/Commits.md)
* [Issue](https://github.com/Desenho2018-2/GitPub/docs/Issues.md)
* [Pull Request](https://github.com/Desenho2018-2/GitPub/docs/Pull-Request.md)

* **Dinâmica 1**
  * [Priorização Documentos](https://github.com/Desenho2018-2/GitPub/docs/Priorizacao-Documentos)
  * [Rich Picture](https://github.com/Desenho2018-2/GitPub/docs/Richpicture.md)
  * [Argumentação](https://github.com/Desenho2018-2/GitPub/docs/Argumentacao.md)
  * [Brainstorm](https://github.com/Desenho2018-2/GitPub/wiki/Brainstorm)
  * [Questionário](https://github.com/Desenho2018-2/GitPub/docs/Questionario.md)
  * [Moscow](https://github.com/Desenho2018-2/GitPub/docs/Moscow.md)
  * [Storytelling](https://github.com/Desenho2018-2/GitPub/docs/Storytelling.md)
  * [Cenários](https://github.com/Desenho2018-2/GitPub/docs/cenarios.md)
  * [Lexicos](https://github.com/Desenho2018-2/GitPub/docs/Lexicos.md)
  * [Protótipo Baixa Fidelidade](https://github.com/Desenho2018-2/GitPub/docs/Prototipo-Baixa-Fidelidade.md)

* **Dinâmica 2**
  * [Metodologia](https://github.com/Desenho2018-2/GitPub/docs/Metodologia.md)

* **Dinâmica 3**
  * [Product Backlog](https://github.com/Desenho2018-2/GitPub/docs/Product-Backlog.md)
  * [Diagrama de caso de uso](https://github.com/Desenho2018-2/GitPub/docs/Diagrama-caso-de-uso.md)
  * [Diagrama de Classe](https://github.com/Desenho2018-2/GitPub/docs/Diagrama-de-Classe.md)
  * [Diagrama Sequencial](https://github.com/Desenho2018-2/GitPub/docs/Diagrama-Sequencial.md)
  * [iStar](https://github.com/Desenho2018-2/GitPub/docs/iStar.md)
    * [Strategic Dependency](https://github.com/Desenho2018-2/GitPub/docs/Strategic-Dependency.md)
    * [Strategic Rationale](https://github.com/Desenho2018-2/GitPub/docs/Strategic-Rationale.md)
  * [NFR Framework](https://github.com/Desenho2018-2/GitPub/docs/NFRFramework.md)
  * [Banco de Dados](https://github.com/Desenho2018-2/GitPub/docs/banco-de-dados.md)
  * [Protótipo Alta Fidelidade]()
