Data|Autor(es)|Descrição|Versão
-|-|-|-
28/08/2018|Kamilla Costa|Adicionando StoryTelling Criação/visualização disciplina|0.1
## Sumário
1. [Introdução](#1-introdução)  
2. [Storytelling Criação e Visualização de disciplinas](#2-storytelling-criação-e-visualização-de-disciplinas)

## 1. Introdução
<p align="justify">A realização desta técnica de elicitação tem como objetivo a identificação de requisitos da aplicação GitPub, sejam eles funcionais ou não funcionais. Com a produção dos artefatos, pretende-se tornar claras as intenções de alguns tipos de usuários com a utilização da aplicação e, assim, detectar funcionalidades e critérios de qualidade relevantes para o sistema.

## 2. Storytelling Criação e Visualização de disciplinas
[![](https://uploaddeimagens.com.br/images/001/584/003/original/story1.png?1535496130)](https://uploaddeimagens.com.br/images/001/584/003/original/story1.png?1535496130)
[![](https://uploaddeimagens.com.br/images/001/584/004/original/story2.png?1535496233)](https://uploaddeimagens.com.br/images/001/584/004/original/story2.png?1535496233)