### Histórico de Revisões

| Data       | Versão | Descrição            |         Autor             |
|:----------:|:------:|:--------------------:|:-------------------------:|
| 19/08/2018 | 0.1 | Criação do Documento com introdução  | [Arthur Diniz](https://github.com/arthurbdiniz) |
| 02/09/2018 | 0.2 | Realocação da explicação teórica da página  | Romeu Antunes |

---

## Versão 1
![richpicture](https://user-images.githubusercontent.com/18387694/44638207-0a697880-a98c-11e8-9f63-7c4182bd9bdb.jpg)
